# ESKF #

* This repo aims to solve a self-contained robot localization problem. 
* Error state Kalman filter is used to fuse different sensors, such as IMU, visual-odometry, optical flow, height sensor, etc.
* The final version should provide 6DOF pose and velocity. 
* The implementation is set up to be a general framework[1], i.e. user should be able to add sensor with minimum coding effort.

# Progress #

* Currently it only has attitude estimation using accelerometer, gyroscope and magnetometer[2].
* [Video](https://youtu.be/SPNKIqhjYYE)

![eskf.jpg](https://bitbucket.org/repo/gjy9r8/images/192290002-eskf.jpg)

# Next Step #

* Implement visual odometry and fuse it, either using landmark or not.
* Use PX4flow and lidar-lite first

# Environment #

* Ubuntu 14.04
* ROS Indigo

# Reference #

[1] ROS ethzasl_sensor_fusion [link](http://wiki.ros.org/ethzasl_sensor_fusion)

[2] Gareth Cross from Kumar Robotics [link](https://github.com/KumarRobotics/kr_attitude_eskf)

[3] Indirect Kalman Filter for 3D Attitude Estimation [link](http://www-users.cs.umn.edu/~trawny/Publications/Quaternions_3D.pdf)