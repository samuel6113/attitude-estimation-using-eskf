#ifndef ATT_ESKF_NODE_HPP
#define ATT_ESKF_NODE_HPP

// ROS
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Quaternion.h>
// related classes
#include "att_eskf/att_eskf.hpp"


class AttESKFNode
{
public:
    AttESKFNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh);
    ~AttESKFNode();

private:

    ros::NodeHandle     nh_;
    
    ros::Subscriber     imu_sub_;
    ros::Subscriber     mag_sub_;
    
    ros::Publisher      quat_pub_;
    ros::Publisher      rpy_pub_;

    ros::Time           prevStamp_;
    
    AttESKF             eskf_;

    float               param_gyro_cov_;
    float               param_acc_cov_;
    float               param_mag_cov_;

    void ImuCB(const sensor_msgs::Imu::ConstPtr &imu);

    void MagCB(const sensor_msgs::MagneticField::ConstPtr &imu);

    bool IsAccelerating(const Vector3d &acc);
};

#endif 