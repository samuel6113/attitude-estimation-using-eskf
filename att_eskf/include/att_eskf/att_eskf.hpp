#ifndef ATT_ESKF_HPP
#define ATT_ESKF_HPP

// ROS
#include <geometry_msgs/Vector3Stamped.h>
// Helper
#include "att_eskf/helper/integrator.hpp"
#include "att_eskf/helper/matrix.hpp"
// TF
#include <tf/transform_broadcaster.h>
// Math
#include <cmath>

// TODO undefined reference to
static constexpr double     kPI = 3.1415926;
static constexpr double     kGravity = 9.81;

// TODO: cannot use static const int in this case
// "this" is not constant expression in template function
#define N_STATE 3


class AttESKF
{
public:
    AttESKF();
    ~AttESKF();

    void Predict(const Vector3d &gyro_data, const double &dt);\

    void Init(const std::string &sensor, const Vector3d &sensor_data);

    void ApplyAttSensor(const std::string &sensor, const Vector3d &sensor_data);

    Quaternion GetQuaternion() { return q_; };

    geometry_msgs::Vector3Stamped GetRPY() { return rpy_; };

    void SetMagnetVector(const Vector3d &mag) { MagVec_ = mag; }; 

    void SetGravityVector(const Vector3d &gravity) { GraVec_ = gravity; }; 
    
    void SetMagCov (const float &data) { mag_cov_ = data; }; 

    void SetAccCov (const float &data) { acc_cov_ = data; }; 

    void SetGyroCov(const float &data) { gyro_cov_ = data; }; 

private:

    /********************/
    /* member variables */
    /********************/
    /* States and covariance */
    Quaternion          q_;
    Vector3d            b_;
    Matrix3d            P_;
    
    /* data */
    Vector3d            w_;
    Vector3d            MagVec_;
    Vector3d            GraVec_;

    /* some coefficients */
    float               bias_threshold_;
    float               gyro_cov_;
    float               acc_cov_;
    float               mag_cov_;
    int                 steady_count_;
    bool                initialized_;

    geometry_msgs::Vector3Stamped   rpy_;
    
    // static constexpr int         nState_;

    /********************/
    /* member functions */
    /********************/
    void BiasEstimation(const Vector3d &gyro);

    void Quat2RPY(const Quaternion &quat, double &roll, double &pitch, double &yaw);

public:
    // The implementation of a non-specialized template must be visible to a translation unit that uses it.
    // http://stackoverflow.com/questions/10632251/undefined-reference-to-template-function
    template<class H_type, class R_type, class r_type>
    void Update(const std::string &sensor, 
                            const Eigen::MatrixBase<H_type> &mJacobian, 
                            const Eigen::MatrixBase<R_type> &mNoise, 
                            const Eigen::MatrixBase<r_type> &mResidual)
    {
        EIGEN_STATIC_ASSERT_FIXED_SIZE(H_type);
        EIGEN_STATIC_ASSERT_FIXED_SIZE(R_type);
        EIGEN_STATIC_ASSERT_FIXED_SIZE(r_type);

        const R_type S = mJacobian * P_ * mJacobian.transpose() + mNoise;

        // ensure numerical stability
        Eigen::FullPivHouseholderQR<R_type> qr(S);
        R_type Sinv = qr.inverse();

        Eigen::Matrix<double, N_STATE, R_type::RowsAtCompileTime> K;
        K = P_ * mJacobian.transpose() * Sinv;

        // use this instead of Vector3d for generalization
        Eigen::Matrix<double, N_STATE, 1> y = K * mResidual;

        P_ = (eye3 - K * mJacobian) * P_;

        if (sensor == "ACC")
            q_ = q_ * Quaternion(1, y[0]/2, y[1]/2, 0);
        else if (sensor == "MAG")
            q_ = q_ * Quaternion(1, 0, 0, y[2]/2);
        q_.normalize();

        // display purpose
        // unless you can read quaternion
        double roll, pitch, yaw;
        Quat2RPY(q_, roll, pitch, yaw);

        rpy_.vector.x = roll*180/kPI;
        rpy_.vector.y = pitch*180/kPI;
        rpy_.vector.z = yaw*180/kPI;
    }       
};

#endif 