#ifndef INTEGRATOR_HPP
#define INTEGRATOR_HPP

#include "att_eskf/helper/matrix.hpp"

static inline Quaternion operator* (const Quaternion &q, const double scale)
{
    return Quaternion(q.w()*scale, q.x()*scale, q.y()*scale, q.z()*scale);
}


static inline Quaternion operator+ (const Quaternion &q1, const Quaternion &q2)
{
    return Quaternion(q1.w()+q2.w(), q1.x()+q2.x(), q1.y()+q2.y(), q1.z()+q2.z());
}


// the order of multiplication matters
// review Trawny paper for more detail
static void integrateRK4(Quaternion &q, const Vector3d &w, const float &dt)
{
    Quaternion k1, k2, k3, k4, qDot(0, w(0), w(1), w(2));

    // k1 = 0.5 * qDot * q;
    // k2 = 0.5 * qDot * (q + 0.5 * dt * k1);
    // k3 = 0.5 * qDot * (q + 0.5 * dt * k2);
    // k4 = 0.5 * qDot * (q + dt * k3);

    // q = q + (dt / 6) * (k1 + 2*k2 + 2*k3 + k4);

    k1 = q * qDot * 0.5;
    k2 =(q + k1 * dt * 0.5) * qDot * 0.5;
    k3 =(q + k2 * dt * 0.5) * qDot * 0.5;
    k4 =(q + k3 * dt) * qDot * 0.5;

    q = q + (k1 + k2*2 + k3*2 + k4) * (dt / 6);
    q.normalize();
}

#endif 
