#ifndef MATRIX_HPP
#define MATRIX_HPP

// Math
#include <Eigen/Dense>  // cmake_modules
#include <Eigen/Eigen>

typedef Eigen::Matrix<double, 3, 3>             Matrix3d;
typedef Eigen::Matrix<double, 3, 1>             Vector3d;

typedef Eigen::Quaternion<double>               Quaternion;

static const Matrix3d eye3 = Matrix3d::Identity();

static Matrix3d skew(const Vector3d &w)
{
    Matrix3d W;

    W(0, 0) = 0;
    W(0, 1) = -w(2);
    W(0, 2) = w(1);

    W(1, 0) = w(2);
    W(1, 1) = 0;
    W(1, 2) = -w(0);

    W(2, 0) = -w(1);
    W(2, 1) = w(0);
    W(2, 2) = 0;

    return W;
}


#endif 
