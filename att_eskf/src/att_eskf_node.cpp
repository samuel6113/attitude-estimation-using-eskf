#include <ros/ros.h>
#include "att_eskf/att_eskf_node.hpp"


AttESKFNode::AttESKFNode(const ros::NodeHandle &nh, const ros::NodeHandle &pnh) :
    nh_(pnh),
    param_gyro_cov_(0.1),
    param_acc_cov_(0.04),
    param_mag_cov_(0.1)
{
    imu_sub_ = nh_.subscribe("/mavros/imu/data_raw", 1, &AttESKFNode::ImuCB, this);
    mag_sub_ = nh_.subscribe("/mavros/imu/mag", 1, &AttESKFNode::MagCB, this);

    quat_pub_ = nh_.advertise<geometry_msgs::Quaternion>("quaternion",1);
    rpy_pub_  = nh_.advertise<geometry_msgs::Vector3Stamped>("rpy",1);

    if (nh_.getParam("magnetometer_covariace", param_mag_cov_))
    {
        eskf_.SetMagCov(param_mag_cov_);
        ROS_INFO("magnetometer covariace set to %f", param_mag_cov_);
    }

    if (nh_.getParam("accelerometer_covariace", param_acc_cov_))
    {
        eskf_.SetAccCov(param_acc_cov_);
        ROS_INFO("accelerometer covariace set to %f", param_acc_cov_);
    }

    if (nh_.getParam("gyroscope_covariace", param_gyro_cov_))
    {
        eskf_.SetGyroCov(param_gyro_cov_);
        ROS_INFO("gyroscope covariace set to %f", param_gyro_cov_);
    }

}


AttESKFNode::~AttESKFNode()
{

}

void AttESKFNode::ImuCB(const sensor_msgs::Imu::ConstPtr &imu)
{
    /* GYRO */
    Vector3d gyro_data;
    gyro_data << imu->angular_velocity.x, imu->angular_velocity.y, imu->angular_velocity.z;


    ros::Duration time = imu->header.stamp - prevStamp_;
    eskf_.Predict(gyro_data, time.toSec());

    prevStamp_ = imu->header.stamp;

    /* ACCELEROMETER */
    Vector3d acc_data;
    acc_data << imu->linear_acceleration.x, imu->linear_acceleration.y, imu->linear_acceleration.z;

    if (!IsAccelerating(acc_data))
        eskf_.ApplyAttSensor("ACC", acc_data);
        
    /* publish result via tf tree for visualization */
    rpy_pub_.publish(eskf_.GetRPY());

    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin(tf::Vector3(0,0,2));
    tf::Quaternion q(eskf_.GetQuaternion().x(), eskf_.GetQuaternion().y(), eskf_.GetQuaternion().z(), eskf_.GetQuaternion().w());
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "imu"));
}


void AttESKFNode::MagCB(const sensor_msgs::MagneticField::ConstPtr &mag)
{
    /* MAGNETOMETER */
    Vector3d mag_data;
    mag_data << mag->magnetic_field.x, mag->magnetic_field.y, mag->magnetic_field.z;

    eskf_.ApplyAttSensor("MAG", mag_data);
}


// use variance to make sure the imu is not being accelerated
// otherwise the attitude estimation will not be accurate
bool AttESKFNode::IsAccelerating(const Vector3d &acc)
{
    static int count = 0;
    static const int filter_size = 20;
    static double sum, avg, variance;

    if (count < filter_size){
        double square_sum = acc(0)*acc(0) + acc(1)*acc(1) + acc(2)*acc(2);
        sum += sqrt(square_sum);
        variance = (sqrt(square_sum)-avg) * (sqrt(square_sum)-avg);
        count++;
    }else{
        avg = sum / static_cast<double>(filter_size);
        count = 0;
        sum = 0;
    }

    if (variance < 0.2)
        return false;
    else
        return true;
}


int main (int argc, char** argv)
{
    ros::init(argc, argv, "att_eskf_node");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    AttESKFNode eskf_node(nh, pnh);

    ros::spin();

    return 0;
}

