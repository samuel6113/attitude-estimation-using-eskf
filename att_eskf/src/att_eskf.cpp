#include "att_eskf/att_eskf.hpp"

AttESKF::AttESKF() : 
    q_(1,0,0,0),
    MagVec_(0.57893-5, 3.164e-5, -1.413e-5),
    GraVec_(0,0,kGravity),
    steady_count_(0),
    bias_threshold_(0.01),
    gyro_cov_(0.1),
    acc_cov_(0.04),
    mag_cov_(0.1),
    initialized_(false)
{
    P_.setIdentity();
    b_.setZero();
    w_.setZero();
}


AttESKF::~AttESKF()
{
    return;
}


void AttESKF::Predict(const Vector3d &gyro_data, const double &dt)
{
    BiasEstimation(gyro_data);
    // true = measuremnt - bias
    w_ = gyro_data - b_;

    // state-transition matrix (Trawny eq. 184)
    // TODO approximate to second order
    // compare it with first order
    const Matrix3d F = eye3 - skew(w_)*dt;

    // noise matrix G (Trawny eq. 203)
    const Matrix3d G = -eye3*dt;

    // this is crucial
    integrateRK4(q_, w_, dt);

    const Matrix3d gCov = eye3*gyro_cov_;

    P_ = F*P_*F.transpose() + G*gCov*G.transpose();

}


void AttESKF::Init(const std::string &sensor, const Vector3d &sensor_data)
{
    static bool acc_inited, mag_inited;
    static float roll, pitch, yaw;

    if (sensor == "ACC" && !acc_inited){
        roll    = std::asin (sensor_data(1)/kGravity);
        pitch   = std::atan2(sensor_data(0),sensor_data(2));

        acc_inited = true;
    }
    // TODO check condition, might prvent getting mag before acc
    else if (sensor == "MAG" && !mag_inited){
        Matrix3d R_BinW = q_.conjugate().matrix();

        Vector3d mPred = R_BinW * MagVec_;

        double earth_north = std::atan2(mPred(1), mPred(0));
        double imu_north   = std::atan2(sensor_data(1), sensor_data(0));

        yaw = imu_north - earth_north;

        mag_inited = true;
    } 

    initialized_ = mag_inited && acc_inited;

    if (initialized_){
        // TODO: ZYX or XYZ, ,might affect
        // q_ = Quaternion(1,0,0,0) * Eigen::AngleAxis<double>(yaw,   Vector3d(0,0,1))
        //                          * Eigen::AngleAxis<double>(pitch, Vector3d(0,1,0))
        //                          * Eigen::AngleAxis<double>(roll,  Vector3d(1,0,0));

        q_ = Quaternion(1,0,0,0) * Eigen::AngleAxis<double>(roll,  Vector3d(1,0,0))
                                 * Eigen::AngleAxis<double>(pitch, Vector3d(0,1,0))
                                 * Eigen::AngleAxis<double>(yaw,   Vector3d(0,0,1));

        q_.normalize();
    }

}


void AttESKF::ApplyAttSensor(const std::string &sensor, const Vector3d &sensor_data)
{
    const Matrix3d R_BinW = q_.conjugate().matrix();

    Vector3d pred;
    Matrix3d R_cov;

    if (sensor == "ACC"){
        pred = R_BinW * GraVec_;
        R_cov = eye3*acc_cov_;
    }
    else if (sensor == "MAG"){
        pred = R_BinW * MagVec_;
        R_cov = eye3*mag_cov_;
    }

    const Vector3d y = sensor_data - pred;

    const Matrix3d H = skew(pred);

    if (initialized_)
        Update(sensor, H, R_cov, y);
    else
        Init(sensor,sensor_data);
}


// use KumarRobotics' method, from Gareth Cross
void AttESKF::BiasEstimation(const Vector3d &gyro)
{
    double magnitude = gyro(0)*gyro(0) + gyro(1)*gyro(1) + gyro(2)*gyro(2);

    if (magnitude < bias_threshold_*bias_threshold_){
        steady_count_++;
        if (steady_count_ > 20)
            b_ = (b_ * (steady_count_ -1) + gyro) / steady_count_;
    }
    else
        steady_count_ = 0; 
}


void AttESKF::Quat2RPY(const Quaternion &quat, double &roll, double &pitch, double &yaw)
{
    tf::Quaternion q(quat.x(), quat.y(), quat.z(), quat.w());
    q.normalize();
    tf::Matrix3x3 m(q);
    m.getRPY(roll, pitch, yaw);
}
